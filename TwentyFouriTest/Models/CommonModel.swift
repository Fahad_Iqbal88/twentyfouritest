//
//  CommonModel.swift
//  TwentyFouriTest
//
//  Created by Fahad Iqbal on 4/19/19.
//  Copyright © 2019 innoverzgen. All rights reserved.
//

import UIKit
import ObjectMapper

class CommonModel: Mappable {
        

    var page, totalResults, totalPages: Int?
    var results: Any?
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            
            self.page <- map["page"]
            self.totalPages <- map["total_pages"]
            totalResults <- map["total_results"]
            results <- map["results"]
            
        }
    


    
}



