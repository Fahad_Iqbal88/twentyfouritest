//
//  MovieResult.swift
//  TwentyFouriTest
//
//  Created by Fahad Iqbal on 4/20/19.
//  Copyright © 2019 innoverzgen. All rights reserved.
//

import UIKit
import  ObjectMapper
class MovieResult: Mappable {

    var voteCount, id: Int?
    var video: Bool?
    var voteAverage: Double?
    var title: String?
    var popularity: Double?
    var posterPath: String?
    var originalLanguage: String?
    var originalTitle: String?
    var genreIDS: [Int]?
    var backdropPath: String?
    var adult: Bool?
    var overview, releaseDate: String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        self.voteCount <- map["vote_count"]
        self.id <- map["id"]
        self.video <- map["video"]
        self.voteAverage <- map["vote_average"]
        self.title <- map["title"]
        self.popularity <- map["popularity"]
        self.posterPath <- map["poster_path"]
        self.originalLanguage <- map["original_language"]
        self.originalTitle <- map["original_title"]
        self.genreIDS <- map["genre_ids"]
        self.backdropPath <- map["backdrop_path"]
        self.adult <- map["adult"]
        self.overview <- map["overview"]
        self.releaseDate <- map["release_date"]
        
        
    }
}
