//
//  AlertUtils.swift
//  TwentyFouriTest
//
//  Created by Fahad Iqbal on 4/19/19.
//  Copyright © 2019 innoverzgen. All rights reserved.
//

import UIKit
import SVProgressHUD
class AlertUtils {
    
   class func showLoader(withStatus : String? = nil) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        SVProgressHUD.show(withStatus: withStatus)
    }
    
   class func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        SVProgressHUD.popActivity()
    }

}
