//
//  MainViewController.swift
//  TwentyFouriTest
//
//  Created by Fahad Iqbal on 4/16/19.
//  Copyright © 2019 innoverzgen. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage
class MainViewController: UITableViewController,
UISearchResultsUpdating, UISearchBarDelegate {
    
    var searchResults = [MovieResult]()
    var moviesData = [MovieResult]()
    
    var searching = false
    var matches = [Int]()
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialize()
        self.title = "Movie Catalog"
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Reachability.isConnectedToNetwork(){
            self.fetchDataFromServer()
        }else{

            let alertController = UIAlertController(title: "Network!", message: "Internet Connection not Available!", preferredStyle: .alert)
            
            
            self.present(alertController, animated: true, completion: nil)

        }
    }

    func initialize() {
       
        navigationController?.navigationBar.prefersLargeTitles = true
        
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Movie Catalog"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        self.tableView.tableFooterView = UIView()
        
    }
    
    //MARK:- Network call for fetching movies
    func fetchDataFromServer(){
        
        TwentyFourNetworkManager.shared.sendGetRequest(endPoint: APIEndPoints.POPULAR_MOVIES+"?api_key="+APIConstants.apiKEY, params: [:], isErrorRequired: true, isShowLoader: true) { (response, error) in

            if(response?.results != nil){
                self.moviesData = Mapper<MovieResult>().mapArray(JSONObject: response?.results) ?? []
                self.searchResults = self.moviesData
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK:- Filter Movies
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text,
            !searchText.isEmpty {
            searchResults.removeAll()
            self.filterContentForSearchText(searchText)
            searching = true
        } else {
            searching = false
        }
        tableView.reloadData()
    }
    
    func filterContentForSearchText(_ searchText: String) {
        searchResults = self.moviesData.filter({( movie : MovieResult) -> Bool in
            return movie.title?.lowercased().contains(searchText.lowercased()) ?? false
        })
        
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return searching ? searchResults.count : moviesData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath:
        IndexPath) -> UITableViewCell {
        
        let cell:MovieTableViewCell =
            self.tableView.dequeueReusableCell(withIdentifier:
                "MovieTableViewCell", for: indexPath)
                as! MovieTableViewCell
        let movie = searching ? searchResults[indexPath.row] : self.moviesData[indexPath.row]
        cell.lblName.text = movie.title
        
        cell.imgMovie.sd_imageTransition = .fade
        cell.imgMovie.sd_setImage(with: URL(string: "http://image.tmdb.org/t/p/w342\(movie.posterPath ?? "")"), placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions(rawValue: 0), completed: { (img, err, cacheType, imgURL) in
            // code
            cell.activityIndicator.isHidden = true
        })

        return cell
    }
    //MARK:- UITABLEVIEW VIEW DELEGATE FUNCTION
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ShowDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetails" {
            
            let detailViewController = segue.destination
                as! DetailsViewController
            
            let myIndexPath = self.tableView.indexPathForSelectedRow!
            let movie = searching ? searchResults[myIndexPath.row] : self.moviesData[myIndexPath.row]
            detailViewController.movie =
                movie
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        tableView.reloadData()
    }
    
    
    
}

