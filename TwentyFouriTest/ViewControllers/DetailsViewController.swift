//
//  DetailsViewController.swift
//  TwentyFouriTest
//
//  Created by Fahad Iqbal on 4/20/19.
//  Copyright © 2019 innoverzgen. All rights reserved.
//

import UIKit
import SDWebImage
class DetailsViewController: UIViewController {

    @IBOutlet weak var imgMovie: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    var movie:MovieResult?
    
    @IBOutlet weak var lblGenre: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblOverview: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Movie Detail"
        // Do any additional setup after loading the view.

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.lblTitle.text = self.movie?.title
        
        self.imgMovie.sd_setImage(with: URL(string: "http://image.tmdb.org/t/p/w342\(self.movie?.posterPath ?? "")"), placeholderImage: UIImage(named: "placeholder"),options: SDWebImageOptions(rawValue: 0), completed: { (img, err, cacheType, imgURL) in
            // code
        })
        
        
    }

    @IBAction func btnMovieAction(_ sender: Any) {
        
        
        
    }

}
