//
//  ENTErrorManager.swift
//  TwentyFouriTest
//
//  Created by Fahad Iqbal on 4/16/19.
//  Copyright © 2019 innoverzgen. All rights reserved.
//

import Foundation
import Alamofire

struct ENTErrorManager {
    
    //MARK: Parsing Error Methods
    static func parseError(error: Error, responseData:Data?, completion: @escaping(NSError)->Void) {
    
        let nsError = error as NSError
        
        do{
            if let data = responseData {
                let json = try JSONSerialization.jsonObject(with: data, options:.mutableContainers) as! [String : AnyObject]
                if let message = json["message"] as? String , let code = json["code"] as? Int {
                    completion(NSError(domain: nsError.domain, code: code, userInfo: [NSLocalizedDescriptionKey: message]))
                }
                else {
                    completion(getGenaricError())
                }
            } else {
                var localizedMessage = ""
                if nsError.code == -1004 || nsError.code == -1009 || nsError.code == -1003 {
                    localizedMessage = "Your Internet connection appears to be offline. Please try again later."
                } else {
                    localizedMessage = "Something went wrong"
                }
                completion(NSError(domain: nsError.domain, code: nsError.code, userInfo: [NSLocalizedDescriptionKey: localizedMessage]))
            }
        } catch {
            let error = getGenaricError()
            completion(error)
        }
    }
    
    static func getGenaricError() -> NSError {
        return NSError(domain: "Entertainer", code: 10000, userInfo: [NSLocalizedDescriptionKey: "Something went wrong"])
    }
    

}

extension NSError {
    func isSignoutError() -> Bool {
        return self.code == 403
    }
}



