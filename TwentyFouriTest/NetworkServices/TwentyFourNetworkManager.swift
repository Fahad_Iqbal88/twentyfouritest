//
//  ENTAlamoFireLayer.swift
//  TwentyFouriTest
//
//  Created by Fahad Iqbal on 4/16/19.
//  Copyright © 2019 innoverzgen. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class TwentyFourNetworkManager : NSObject {
    static let shared : TwentyFourNetworkManager = TwentyFourNetworkManager()
    
    private override init() {
        
    }
    
    func cancelAllRequests() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
    
    func cancelRequestFor(url: String) {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach {
                if ($0.originalRequest?.url?.absoluteString == url) { $0.cancel() }
            }
            uploadTasks.forEach {
                if ($0.originalRequest?.url?.absoluteString == url) { $0.cancel() }
            }
            downloadTasks.forEach {
                if ($0.originalRequest?.url?.absoluteString == url) { $0.cancel() }
            }
        }
    }
    
    
    func sendGetRequest(endPoint: String, params: Parameters?, isErrorRequired: Bool, isShowLoader: Bool, completion:@escaping (_ response : CommonModel?, _ error : NSError?) -> Void ) {
        
        self.sendRequestForServer(method: .get, path: endPoint, params: params, isErrorRequired: isErrorRequired, isShowLoader: isShowLoader) { (response, error) in
            completion(response, error)
            return
        }
    }
    
   
    
    private func sendRequestForServer(method : HTTPMethod,path: String, params: Parameters?,  isErrorRequired: Bool, isShowLoader: Bool, completion:@escaping (_ response : CommonModel?, _ error : NSError?) -> Void ) {
        
        
        if isShowLoader {
            AlertUtils.showLoader()
        }
        
        // for the time being we add this condition because some API's is not working with JSONEncoding
            Alamofire.request(APIConstants.baseURL+path,
                              method: method,
                              parameters:params,
                              encoding:JSONEncoding.default).responseData
                { response in
                    
                    AlertUtils.hideLoader()
                    var commonModel:CommonModel? = nil
                    switch response.result {
                        
                    case .success:
                            if let data = response.data, let responsString = String(data: data, encoding: .utf8) {
                                commonModel = Mapper<CommonModel>().map(JSONString: responsString)
                                }
                        completion(commonModel, nil)
                        return
                    case .failure(let error):
                        ENTErrorManager.parseError(error: error, responseData: response.data, completion: { (error) in
                            
                            if !isErrorRequired {
                                AlertUtils.hideLoader()
                            } else {
                                completion(nil, error)
                            }
                        })
                    }
            }
        }
        
        
}
